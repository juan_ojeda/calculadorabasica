<?php
    class Calculadora{
        public $nro1=1;
        public $nro2=1;
        // Metodos u operaciones
        public function Sumar()
        {
            return $this->nro1 + $this->nro2;
        }

        public function Restar()
        {
            return $this->nro1 - $this->nro2;
        }

        // Multiplicar y dividir (Validar entre 0)

        private function Fact($nro)
        {
            if($nro == 0)
                return 1;
            else
                return $nro * $this->Fact($nro - 1);
        }

        public function Factorial()
        {
            return $this->Fact($this->nro1);
        }

        public function mult(){
            return $this->nro1 * $this->nro2;
        }

        public function div(){
            return $this->nro1 / $this->nro2;
        }

        // Potencia, Seno, Tangente.
        private function potencia($b,$ex){
            if ($ex==0){
                return 1;
            }
            else {
                return $b * $this->potencia($b,$ex-1);
            }
        }
        public function potencia2(){
            return $this->potencia($this->nro1,$this->nro2);
        }

        private function sen($n1){
            return (sin(($n1*pi())/180));
        }
       
        private function tan($n1){
            return tan(($n1*pi())/180);
        }
        public function tangente(){
            return $this->tan($this->nro1);
        }
        public function seno(){
            return $this->sen($this->nro1);
        }

        private function PorcentajeCal($porc,$num){
            return (($porc*$num)/100);
        }
        public function Porcentaje(){
            return $this->PorcentajeCal($this->nro1,$this->nro2);
        }

        private function RaizCuadradaCal($n1){
            return (sqrt($n1));
        }
        public function RaizCuadrada(){
            return $this->RaizCuadradaCal($this->nro1);
        }

        private function RaizCal($n1,$po){
            return pow($n1,(1/$po));
        }
        public function Raiz(){
            return $this->RaizCal($this->nro1,$this->nro2);
        }

        private function inversaCal($num){
            return (pow($num,-1));
        }
        public function inversa(){
            return $this->inversaCal($this->nro1);
        }
    }
?>