<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculadora</title>
    <link rel="stylesheet" href="./sass/custom.css">
</head>
<body>
    
    <div class="container">
        <form action="#" method="POST">
            <legend>❤ Calculadora ❤</legend>
            <div class="row">
                <div class="col-12 col-sm-6 text-success">
                <P>Numero 1 <input type="text" name="txtNr1" class="form-control" ></P>
                </div>
                <div class="col-12 col-sm-6 text-success">
                <P>Numero 2 <input type="text" name="txtNr2" class="form-control" ></P>
                </div>
            </div>
            <br>
            <div class="row ">
                <div class="col-4 col-sm-3 col-md-2 col-lg-1 "><input type="submit" name="btnSumar" value="+" class="btn btn-outline-dark"></div>
                <div class="col-4 col-sm-3 col-md-2 col-lg-1"><input type="submit" name="btnResta" value="-" class="btn btn-outline-dark"></div>
                <div class="col-4 col-sm-3 col-md-2 col-lg-1"><input type="submit" name="btnMultiplicacion" value="*" class="btn btn-outline-dark"></div>
                <div class="col-4 col-sm-3 col-md-2 col-lg-1"><input type="submit" name="btnDivision" value="/" class="btn btn-outline-dark"></div>
                <div class="col-4 col-sm-3 col-md-2 col-lg-1"><input type="submit" name="btnFactorial" value="!" class="btn btn-outline-dark"></div>
                <div class="col-4 col-sm-3 col-md-2 col-lg-1"><input type="submit" name="btnPorcentaje" value="%" class="btn btn-outline-dark"></div>
                <div class="col-4 col-sm-3 col-md-2 col-lg-1"><input type="submit" name="btnRaizCuad" value="√x" class="btn btn-outline-dark"></div>
                <div class="col-4 col-sm-3 col-md-2 col-lg-1"><input type="submit" name="btnRaiz" value="ª√x" class="btn btn-outline-dark"></div>
                <div class="col-4 col-sm-3 col-md-2 col-lg-1"><input type="submit" name="btnPotencia" value="xª" class="btn btn-outline-dark"></div>
                <div class="col-4 col-sm-3 col-md-2 col-lg-1"><input type="submit" name="btnInversa" value="1/x" class="btn btn-outline-dark"></div>
                <div class="col-4 col-sm-3 col-md-2 col-lg-1"><input type="submit" name="btnSen" value="Sen" class="btn btn-outline-dark"></div>
                <div class="col-4 col-sm-3 col-md-2 col-lg-1"><input type="submit" name="btnTan" value="Tan" class="btn btn-outline-dark"></div>
            </div>
            <br>
            <div class="row">
                <div class="col-12 text-success">
                    <P>Respuesta <input type="txt" name="txtResp" class="form-control" id="txtResp"></P>
                </div>
            </div>
            
        </form>
    </div>

    <?php 
    if(($_POST))
    {
        
        include("calculadora.php");     

        $nro1 = $_POST['txtNr1'];
        $nro2 = $_POST['txtNr2'];


        if ($nro1 != ""){
            
            
            $calculo = new Calculadora;
                $calculo->nro1 = $nro1;
                $calculo->nro2 = $nro2;

            if(isset($_POST['btnSumar']))
            {
                if ($nro2 != ""){
                    ?>
                        <script>
                            document.getElementById("txtResp").value=<?php echo $nro1; ?>+" + "+<?php echo $nro2; ?>+" = "+<?php echo $calculo->Sumar(); ?>
                        </script>
                    <?php
                }
                else {
                    ?>
                        <script>
                                document.getElementById("txtResp").value="Ingrese datos" 
                        </script>
                    <?php
                }
            }
            if(isset($_POST['btnResta']))
            {
                
                if ($nro2 != ""){
                    ?>
                        <script>
                            document.getElementById("txtResp").value=<?php echo $nro1; ?>+" - "+<?php echo $nro2; ?>+" = "+<?php echo $calculo->Restar(); ?>
                        </script>
                    <?php
                }
                else {
                    ?>
                        <script>
                                document.getElementById("txtResp").value="Ingrese datos" 
                        </script>
                    <?php
                }
            }
            if(isset($_POST['btnMultiplicacion']))
            {
                if ($nro2 != ""){
                    ?>
                        <script>
                            document.getElementById("txtResp").value=<?php echo $nro1; ?>+" - "+<?php echo $nro2; ?>+" * "+<?php echo $calculo->mult(); ?>
                        </script>
                    <?php
                }
                else {
                    ?>
                        <script>
                                document.getElementById("txtResp").value="Ingrese datos" 
                        </script>
                    <?php
                }
            }
            if(isset($_POST['btnDivision']))
            {
                if ($nro2 != ""){
                        if($nro2==0){
                        ?>
                            <script>
                                document.getElementById("txtResp").value="ERROR dividision entre 0"
                            </script>
                        <?php
                    }
                    else{
                        ?>
                            <script>
                                document.getElementById("txtResp").value=<?php echo $nro1; ?>+" / "+<?php echo $nro2; ?>+" = "+<?php echo $calculo->div(); ?>
                            </script>
                        <?php
                    }
                }
                else {
                    ?>
                        <script>
                                document.getElementById("txtResp").value="Ingrese datos" 
                        </script>
                    <?php
                }
                
                
            }
            else  if(isset($_POST['btnFactorial']))
            {
                
                ?>
                    <script>
                        document.getElementById("txtResp").value=<?php echo $nro1; ?>+"! = "+<?php echo $calculo->Factorial(); ?>
                    </script>
                <?php
            }    

            if(isset($_POST['btnPotencia']))
            {
                if ($nro2 != ""){
                    ?>
                        <script>
                            document.getElementById("txtResp").value=<?php echo $nro1; ?>+" ^ "+<?php echo $nro2; ?>+" = "+<?php echo $calculo->Potencia2($nro1,$nro2); ?>
                        </script>
                    <?php
                }
                else {
                    ?>
                        <script>
                                document.getElementById("txtResp").value="Ingrese datos" 
                        </script>
                    <?php
                }
                    
            }

            if(isset($_POST['btnSen']))
            {
                ?>
                    <script>
                        document.getElementById("txtResp").value="sen("+<?php echo $nro1; ?>+") = "+<?php echo $calculo->seno($nro1); ?>
                    </script>
                <?php
            }
        
            if(isset($_POST['btnTan']))
            {
                ?>
                    <script>
                        document.getElementById("txtResp").value="tan("+<?php echo $nro1; ?>+") = "+<?php echo $calculo->tangente($nro1); ?>
                    </script>
                <?php
            }
            if(isset($_POST['btnPorcentaje']))
            {
                
                if ($nro2 != ""){
                    ?>
                        <script>
                            document.getElementById("txtResp").value="El "+<?php echo $nro2; ?>+"% de "+<?php echo $nro1; ?>+" = "+<?php echo $calculo->Porcentaje($nro1,$nro2); ?>
                        </script>
                    <?php
                }
                else {
                    ?>
                        <script>
                                document.getElementById("txtResp").value="Ingrese datos" 
                        </script>
                    <?php
                }
            }

            if(isset($_POST['btnRaizCuad']))
            {
                ?>
                    <script>
                        document.getElementById("txtResp").value="√"+<?php echo $nro1; ?>+" = "+<?php echo $calculo->RaizCuadrada($nro1); ?>
                    </script>
                <?php
            }

            if(isset($_POST['btnRaiz']))
            {
                
                if ($nro2 != ""){
                    ?>
                        <script>
                            document.getElementById("txtResp").value=<?php echo $nro2; ?>+"√"+<?php echo $nro1; ?>+" = "+<?php echo $calculo->RaizCuadrada($nro1); ?>
                        </script>
                    <?php
                }
                else {
                    ?>
                        <script>
                                document.getElementById("txtResp").value="Ingrese datos" 
                        </script>
                    <?php
                }
            }

            if(isset($_POST['btnInversa']))
            {
                ?>
                    <script>
                        document.getElementById("txtResp").value="Inversa 1/"+<?php echo $nro1; ?>+" = "+<?php echo $calculo->inversa($nro1); ?>
                    </script>
                <?php
            }
        }
        else{
            ?>
                <script>
                        document.getElementById("txtResp").value="Ingrese datos" 
                </script>
            <?php
        }
    }
    ?>
    
</body>
</html>